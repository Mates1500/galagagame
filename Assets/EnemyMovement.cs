﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {
	float timeCounter=0;
//	float timeCounter2=0;
//	Vector2 target = new Vector2(0,2);
	public GameObject player;
//	bool timeFlag  = false;
	int circleComplete  = 0;
	public float speed; //let's set the value in the editor for easy manipulation 
	float totaldistance;
//	Vector2 enemypos = new Vector2(0,0);
	Vector2 lastmovingpos = new Vector2(0,0);
	int distanceFlag;

	float enemyangle;
	public Sprite sprite1;
	public Sprite sprite2;
	public float timeToChangeSprite;
	bool changeToSprite2;
	float spriteChangeTimer;
	float angleOffset;
	bool firstSpawnMovementFinished; //first spawn movement - getting to the center of a specific lane
	bool secondSpawnMovementFinished; //second spawn movement - finding a spot in the lane
	public float preDeterminedXInLane;
	int preDeterminedLane; //which lane to spawn in, unused so far

	bool afterFirstStep;
	// Use this for initialization
	void Start () {
		firstSpawnMovementFinished = false;
		secondSpawnMovementFinished = false;
		afterFirstStep = false;
		changeToSprite2 = true;
		spriteChangeTimer = timeToChangeSprite;
		totaldistance = Vector2.Distance(transform.position,player.transform.position);
		/*--movement-*/
		enemyangle = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(!afterFirstStep) afterFirstStep = true;
		timeCounter += Time.deltaTime;
		//Vector2 offset = new Vector2(1,1);

		spriteChangeTimer -= Time.deltaTime;
		if(spriteChangeTimer<=0)
		{
			spriteChangeTimer = timeToChangeSprite; //reset the timer
			ChangeSprite(changeToSprite2);
			changeToSprite2 = changeToSprite2 ? false : true; //shorter statement to switch the thing back and forth, because fuck 8 lines if it can be on one instead
		}

	
		float curDistance = Vector2.Distance(transform.position,player.transform.position);
		if(curDistance < totaldistance/2  && circleComplete == 0){
			distanceFlag=1;
		}

	if(!firstSpawnMovementFinished)
	{
		if(circleComplete== distanceFlag){
			enemytraslation(Time.deltaTime);
			angleOffset =transform.rotation.eulerAngles.z;
		}else{
			enemyrotation(Mathf.PI/20,2*Mathf.PI,speed*4*Time.deltaTime);
			float x = speed*Time.deltaTime*Mathf.Cos(enemyangle)+ transform.position.x;
			float y = speed*Time.deltaTime*Mathf.Sin(enemyangle)+ transform.position.y;

			transform.position = new Vector2(x,y);

			transform.eulerAngles = new Vector3(0,0,Mathf.Rad2Deg*enemyangle + angleOffset);

		}
	}

		if(!firstSpawnMovementFinished)
		{
			if(curDistance == 0)
			{
				firstSpawnMovementFinished = true;
				//Debug.Log("Spawn Movement finished.");
			}
		}

		if(firstSpawnMovementFinished && !secondSpawnMovementFinished)
		{

			if(preDeterminedXInLane != null)
			{
				float step = speed * Time.deltaTime;
				transform.position = Vector2.MoveTowards(transform.position, new Vector2(preDeterminedXInLane, player.transform.position.y), step);
			}
			if(transform.position.x == preDeterminedXInLane)
			{
				secondSpawnMovementFinished = true;
				//Debug.Log("Second spawn movement finished");
			}
		}
//
//		

	}

	/*---movement-*/
	void enemyrotation(float movAngle, float maxAngle,float deltatime){
		enemyangle += movAngle*deltatime;
		//Debug.Log("enemyangle"+enemyangle);
		if(maxAngle > 0){
			if(enemyangle >= maxAngle){
				circleComplete++;
			}
		}else{
			if(enemyangle <= maxAngle){
				circleComplete++;
			}
		}
		
	}

	void enemytraslation(float deltatime){

		float step = speed * deltatime;
		transform.position = Vector2.MoveTowards(transform.position,player.transform.position,step);
		RotateBasedOnAngle(transform.position, player.transform.position);
	}
	/*---*/
	void RotateBasedOnAngle(Vector2 v1, Vector2 v2)
	{
		float angle = Vector2.Angle(v1, v2);
		//Debug.Log("Angle for rotation:" + angle + ", Enemy rotation:" + gameObject.transform.eulerAngles.z);
		gameObject.transform.Rotate(0, 0, angle-gameObject.transform.eulerAngles.z+180); //+180 degrees because otherwise it goes backwards
		//works exactly the opposite way while inside the circle motion, adjustments required
	
	}

	void ChangeSprite(bool sw)
	{
		if (sw)
		{
			gameObject.GetComponent<SpriteRenderer>().sprite = sprite2;
		}
		else
		{
			gameObject.GetComponent<SpriteRenderer>().sprite = sprite1;
		}
	}

	public void SetPreDeterminedX(float x)
	{
		preDeterminedXInLane = x;
	}
}
