﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ObjectManager : MonoBehaviour {
	public GameObject playerBulletPrefab;
	public int bulletsToSpawnTotal;
	public List <GameObject> playerBullets;
	// Use this for initialization
	void Start () {
		for(int i=0; i<bulletsToSpawnTotal-1; i++)
		{
			playerBullets.Add(GameObject.Instantiate(playerBulletPrefab));
			playerBullets[i].SetActive(false);
		}
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
