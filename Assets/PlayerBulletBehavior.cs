﻿using UnityEngine;
using System.Collections;

public class PlayerBulletBehavior : MonoBehaviour {
	public Transform bullet;
	public float bulletSpeed;
	public float outOfBoundsLimit;
	bool flying;
	Vector2 bulletPos;
	PlayerShipBehavior ship;
	// Use this for initialization
	void Start () {
		bullet = gameObject.transform;
		//flying = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(flying)
		{
			bulletPos.y += bulletSpeed * Time.deltaTime;
			bullet.localPosition = bulletPos;
			if(bulletPos.y > outOfBoundsLimit)
			{
				flying = false;
				gameObject.SetActive(false);
				ship.ChangeCurrentBulletsCount(-1);
			}
		}
	}

	public void ShotFromPos(Vector2 pos)
	{
		ship = (PlayerShipBehavior)GameObject.FindGameObjectWithTag("Player").GetComponent(typeof(PlayerShipBehavior));
		bullet = gameObject.transform;
		bullet.localPosition = pos;
		bulletPos = pos;
		flying = true;
		ship.ChangeCurrentBulletsCount(1);
	}
}
