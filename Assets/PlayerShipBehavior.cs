﻿using UnityEngine;
using System.Collections;

public class PlayerShipBehavior : MonoBehaviour {
	public Transform playerShip;
	public float speed;
	public float shootCooldown; //in seconds
	public int bulletLimit;
	int currentBullets;
	float timer;
	Vector2 playerPos;

	ObjectManager om;
	// Use this for initialization
	void Start () {
		playerShip = gameObject.transform;
		om = (ObjectManager)GameObject.FindGameObjectWithTag("OM").GetComponent(typeof(ObjectManager));
		currentBullets=0;
	}
	
	// Update is called once per frame
	void Update () {
		if(timer>0) timer-= Time.deltaTime;

		playerPos = playerShip.localPosition;
		if(Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
		{
			playerPos.x -= speed*Time.deltaTime;
			playerShip.localPosition = playerPos;
		}
		if(Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
		{
			playerPos.x += speed*Time.deltaTime;
			playerShip.localPosition = playerPos;
		}

		if(Input.GetKey(KeyCode.Space))
		{
			//SHOOT CODE
			if(timer <=0 && currentBullets < bulletLimit)
			{
			PlayerBulletBehavior b = FindUnusedBullet();
			b.ShotFromPos(playerPos);
			timer=shootCooldown;
			}
		}
	}

	PlayerBulletBehavior FindUnusedBullet()
	{
		for(int i=0; i<om.playerBullets.Count; i++)
		{
			if(om.playerBullets[i].activeInHierarchy == false)
			{
				om.playerBullets[i].SetActive(true);
				return (PlayerBulletBehavior)om.playerBullets[i].GetComponent(typeof(PlayerBulletBehavior));
			}
		}
		return (PlayerBulletBehavior)om.playerBullets[0].GetComponent(typeof(PlayerBulletBehavior)); //workaround to prevent null errors
	}

	public void ChangeCurrentBulletsCount(int x)
	{
		currentBullets+=x;
	}
}
