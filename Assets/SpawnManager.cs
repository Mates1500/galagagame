﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnManager : MonoBehaviour {
	public GameObject enemyObject;
	public int enemiesToSpawn;
	public float xOffset; //the bare x offset between the enemies
	float xTotalOffset; //takes the scale of the enemy into account
	bool minusSwitch; //first X is 0, afterwards the enemies alternate on the + and - side to make it look seamless
	public List <GameObject> enemies;
	float enemyScaleX;
	float basicDistance = 0.15f;
	int minusMultiplier;
	int numberOfCycles; //increases by one after each spawn to determine the X position
	// Use this for initialization
	void Start () {
		enemyObject = GameObject.Find("enemy");
		Debug.Log(enemyObject.transform.localScale.x);
		numberOfCycles = 1;
		enemyScaleX = enemyObject.transform.localScale.x;
		minusSwitch = false;
		for(int i=0; i<enemiesToSpawn; i++)
		{
			if(i==0) {enemies.Add(enemyObject);}
			else {enemies.Add(GameObject.Instantiate(enemyObject));}
			if(i!=0) //first one is in the center, the next ones actually need to move
			{

				if(!minusSwitch)
				{
					minusMultiplier = 1;
					minusSwitch = true;
				}
				else
				{
					minusMultiplier = -1;
					minusSwitch = false;
				}

				enemies[i].GetComponent<EnemyMovement>().SetPreDeterminedX((basicDistance*enemyScaleX+xOffset)*minusMultiplier*numberOfCycles);
				if(i>=2 && !minusSwitch) numberOfCycles++;
			}
			else
			{
				enemies[i].GetComponent<EnemyMovement>().SetPreDeterminedX(0f);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
